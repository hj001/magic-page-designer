/**
 * 判断arr是否为一个数组，返回一个bool值
 * @param arr
 * @returns
 */
export const isArray = function(arr: any) {
  return Object.prototype.toString.call(arr) === '[object Array]'
}

/**
 * 展示锚点对象
 * @param dom
 */
export const goToAnchor = function(dom: Element | string | null) {
  if (typeof dom === 'string') {
    dom = document.querySelector(dom)
  }
  if (dom) {
    dom.scrollIntoView(true)
  }
}

/**
 * 获取url中的参数
 * @param {String} variable
 * @returns
 */
export const getQueryVariable = function(variable: string) {
  var query = window.location.search.substring(1)
  var vars = query.split('&')
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split('=')
    if (pair[0] == variable) {
      return pair[1]
    }
  }
  return false
}

/**
 * 生成随机uuid
 * @param {Number} len 长度
 * @param {Number} radix 基数
 * @returns
 */
export const uuid = function(len?: number, radix?: number) {
  let chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('')
  let uuid = []
  let i
  radix = radix || chars.length
  if (len) {
    // Compact form
    for (i = 0; i < len; i++) uuid[i] = chars[0 | (Math.random() * radix)]
  } else {
    // rfc4122, version 4 form
    let r
    // rfc4122 requires these characters
    uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-'
    uuid[14] = '4'
    // Fill in random data.  At i==19 set the high bits of clock sequence as
    // per rfc4122, sec. 4.1.5
    for (i = 0; i < 36; i++) {
      if (!uuid[i]) {
        r = 0 | (Math.random() * 16)
        uuid[i] = chars[i == 19 ? (r & 0x3) | 0x8 : r]
      }
    }
  }
  return uuid.join('')
}
/**
 * 递归获取一个对象
 * @param {Array} arr 数组对象
 * @param {string} field 字段
 * @param {any} value 字段值
 * @param {string} childField 字段值
 * @returns
 */
export const loopGetOneItem = function(
  arr: Array<any>,
  field: string,
  value: any,
  childField: string = 'children'
): any {
  if (isArray(arr) && arr.length > 0) {
    for (let index in arr) {
      const item = arr[index]
      if (item[field] === value) {
        return item
      }
      const tmp = loopGetOneItem(item[childField], field, value, childField)
      if (tmp !== null) {
        return tmp
      }
    }
  }
  return null
}
/**
 * 字段转下划线格式:testField -> test_field
 * @param {String} field 转换的内容
 * @param {String} separator 分割用的字符，默认'_'
 * @returns 转换好的内容
 */
export const fieldToUnderscore = function(field: string, separator: string = '_') {
  return field.replace(/([A-Z])/g, separator + '$1').toLowerCase()
}
/**
 * 将下划线分割的字符转成驼峰命名:test_field -> testField
 * @param {String} field 转换的内容
 * @param {String} separator 分割用的字符，默认'_'
 * @param {Boolean} firstLetter 首字母大写，默认false
 * @returns
 */
export const fieldToCamel = (field: string, separator: string = '_', firstLetter: boolean = false) => {
  const str = field.replace(new RegExp(separator + '(\\w)', 'g'), function(all, letter) {
    return letter.toUpperCase()
  })
  if (firstLetter) {
    return str.substr(0, 1).toUpperCase() + str.substr(1)
  } else {
    return str
  }
}
