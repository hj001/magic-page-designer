/**
 * 绑定事件
 *
 * @param dom 绑定的dom对象
 * @param eventType 监听类型
 * @param callback 回调方法
 */
export function addEvent(dom: HTMLElement, eventType: keyof HTMLElementEventMap, callback: any) {
  dom.addEventListener(eventType, callback)
}

/**
 * 解绑事件
 *
 * @param dom 绑定的dom对象
 * @param eventType 监听类型
 * @param callback 回调方法
 */
export function removeEvent(dom: HTMLElement, eventType: keyof HTMLElementEventMap, callback: any) {
  dom.removeEventListener(eventType, callback)
}
