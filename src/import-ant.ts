// antd组件
import {
  Dropdown,
  Menu,
  Tree,
  Input,
  Tag,
  Popover,
  Tabs,
  AutoComplete,
  Button,
  Tooltip,
  Popconfirm,
  Icon,
  Select,
  InputNumber,
  Modal,
  Table,
  Divider
} from 'ant-design-vue'
import Vue from 'vue'

const install = function(app: typeof Vue) {
  // antd组件
  app.use(Dropdown)
  app.use(Menu)
  app.use(Tree)
  app.use(Input)
  app.use(Tag)
  app.use(Popover)
  app.use(Tabs)
  app.use(AutoComplete)
  app.use(Button)
  app.use(Tooltip)
  app.use(Popconfirm)
  app.use(Icon)
  app.use(Select)
  app.use(InputNumber)
  app.use(Modal)
  app.use(Table)
  app.use(Divider)
}

export default {
  install
}
