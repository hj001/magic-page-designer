import { isArray } from '../util/utils'
import { ComponentGroupInfo } from '../library'

// 全局使用的组件库
export const mpdComponentLibrary: Array<ComponentGroupInfo> = []
// 添加全局使用的组件库
export const addLibrary = function(obj: ComponentGroupInfo | Array<ComponentGroupInfo>) {
  obj = isArray(obj) ? <Array<ComponentGroupInfo>>obj : [<ComponentGroupInfo>obj]
  obj.forEach(element => {
    element.list.forEach(item => {
      item.mpdConfig = item.mpdConfig || {}
    })
    mpdComponentLibrary.push(element)
  })
}
