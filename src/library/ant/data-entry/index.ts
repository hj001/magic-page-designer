import { ComponentGroupInfo } from '../../page-object'
import checkbox from './checkbox'
import formModel from './form-model'
import input from './input'
import inputNumber from './input-number'
import radio from './radio'
import select from './select'
import switchCom from './switch'

const data: ComponentGroupInfo = {
  id: 'data-entry',
  title: '数据录入',
  list: [...checkbox, ...formModel, ...input, inputNumber, ...radio, select, switchCom]
}

export default data
