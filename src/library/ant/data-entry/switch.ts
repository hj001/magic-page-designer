import { PageDom, ConfigDetailType } from '@/library/page-object'
import { getBooleanSelect, getSizeSelect } from '@/library/ant'

const data: PageDom = {
  id: 'a-switch',
  title: '开关',
  mpdConfig: {
    slot: []
  },
  ext: {
    componentConfig: [
      {
        type: 'attr',
        title: '自动获取焦点',
        name: 'autoFocus',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'model',
        title: '指定当前是否选中',
        name: 'checked',
        group: 'general',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'attr',
        title: '选中时的内容',
        name: 'checkedChildren',
        group: 'general'
      },
      {
        type: 'attr',
        title: '非选中时的内容',
        name: 'unCheckedChildren',
        group: 'general'
      },
      {
        type: 'attr',
        title: '初始是否选中',
        name: 'defaultChecked',
        group: 'general',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'attr',
        title: '是否禁用',
        name: 'disabled',
        group: 'general',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'attr',
        title: '加载中的开关',
        name: 'loading',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'attr',
        title: '选择框大小',
        name: 'size',
        group: 'general',
        inputList: [getSizeSelect()]
      },
      {
        type: 'event',
        title: '变化时回调函数',
        name: 'change',
        help: 'Function(checked:Boolean, event: Event)'
      },
      {
        type: 'event',
        title: '点击时回调函数',
        name: 'click',
        help: 'Function(checked:Boolean, event: Event)'
      }
    ]
  }
}

export default data
