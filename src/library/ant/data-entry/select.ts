import { PageDom, ConfigDetailType } from '@/library/page-object'
import { getBooleanSelect, getSizeSelect } from '@/library/ant'

const groupName: Array<string> = ['样式']

const data: PageDom = {
  id: 'a-select',
  title: '选择器',
  mpdConfig: {
    slot: []
  },
  ext: {
    componentConfig: [
      {
        type: 'attr',
        title: 'options数据',
        name: 'options',
        help: '如果设置则不需要手动构造 selectOption 节点。array<{value, label, [disabled, key, title]}>'
      },
      {
        type: 'attr',
        title: '提示信息',
        name: 'placeholder',
        group: 'general'
      },
      {
        type: 'attr',
        title: '支持清除',
        name: 'allowClear',
        group: 'general',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'attr',
        name: 'autoClearSearchValue',
        help: '是否在选中项后清空搜索框，只在 mode 为 multiple 或 tags 时有效。',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'attr',
        title: '默认获取焦点',
        name: 'autoFocus',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'attr',
        name: 'defaultActiveFirstOption',
        help: '是否默认高亮第一个选项',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'attr',
        title: '指定默认选中的条目',
        name: 'defaultValue'
      },
      {
        type: 'attr',
        title: '是否禁用',
        name: 'disabled',
        group: 'general',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'attr',
        name: 'dropdownClassName',
        group: groupName[0],
        help: '下拉菜单的 className 属性'
      },
      {
        type: 'attr',
        title: '下拉菜单和选择器同宽',
        name: 'dropdownMatchSelectWidth',
        group: groupName[0],
        inputList: [getBooleanSelect()]
      },
      {
        type: 'attr',
        title: '自定义下拉框内容',
        name: 'dropdownRender'
      },
      {
        type: 'attr',
        title: '下拉菜单的style属性',
        group: groupName[0],
        name: 'dropdownStyle'
      },
      {
        type: 'attr',
        title: 'dropdown菜单自定义样式',
        group: groupName[0],
        name: 'dropdownMenuStyle'
      },
      {
        type: 'attr',
        title: '是否根据输入项进行筛选',
        name: 'filterOption',
        help:
          '当其为一个函数时，会接收 inputValue option 两个参数，当 option 符合筛选条件时，应返回 true，反之则返回 false。',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'attr',
        title: '默认高亮的选项',
        name: 'firstActiveValue'
      },
      {
        type: 'attr',
        title: '菜单渲染父节点',
        name: 'getPopupContainer',
        group: groupName[0],
        help: '默认渲染到 body 上，如果你遇到菜单滚动定位问题，试试修改为滚动的区域，并相对其定位'
      },
      {
        type: 'attr',
        name: 'labelInValue',
        help:
          '是否把每个选项的 label 包装到 value 中，会把 Select 的 value 类型从 string 变为 {key: string, label: vNodes} 的格式'
      },
      {
        type: 'attr',
        title: '最多显示多少个tag',
        name: 'maxTagCount',
        inputList: [
          {
            title: '数字框',
            component: ConfigDetailType.InputNumber
          }
        ]
      },
      {
        type: 'attr',
        title: '隐藏tag时显示的内容',
        name: 'maxTagPlaceholder'
      },
      {
        type: 'attr',
        title: '最大显示的tag文本长度',
        name: 'maxTagTextLength',
        inputList: [
          {
            title: '数字框',
            component: ConfigDetailType.InputNumber
          }
        ]
      },
      {
        type: 'attr',
        title: 'Select的模式',
        name: 'mode',
        group: 'general',
        help: '设置 Select 的模式为多选或标签',
        inputList: [
          {
            title: 'Select',
            component: ConfigDetailType.Select,
            attr: {
              allowClear: true,
              options: [
                {
                  value: 'default',
                  label: 'default'
                },
                {
                  value: 'multiple',
                  label: 'multiple'
                },
                {
                  value: 'tags',
                  label: 'tags'
                },
                {
                  value: 'combobox',
                  label: 'combobox'
                }
              ]
            }
          }
        ]
      },
      {
        type: 'attr',
        title: '下拉列表为空时的内容',
        name: 'notFoundContent'
      },
      {
        type: 'attr',
        name: 'optionFilterProp',
        help: '搜索时过滤对应的 option 属性，如设置为 children 表示对内嵌内容进行搜索'
      },
      {
        type: 'attr',
        name: 'optionLabelProp',
        help:
          '回填到选择框的 Option 的属性值，默认是 Option 的子元素。比如在子元素需要高亮效果时，此值可以设为 value。（combobox 模式下为 value）'
      },
      {
        type: 'attr',
        title: '使单选模式可搜索',
        name: 'showSearch',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'attr',
        title: '是否显示下拉小箭头',
        name: 'showArrow',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'attr',
        title: '选择框大小',
        name: 'size',
        group: 'general',
        inputList: [getSizeSelect()]
      },
      {
        type: 'attr',
        title: '选择框后缀图标',
        name: 'suffixIcon',
        group: groupName[0],
        help: 'VNode | slot'
      },
      {
        type: 'attr',
        title: '多选框清除图标',
        name: 'removeIcon',
        group: groupName[0],
        help: 'VNode | slot'
      },
      {
        type: 'attr',
        title: '多选框清空图标',
        name: 'clearIcon',
        group: groupName[0],
        help: 'VNode | slot'
      },
      {
        type: 'attr',
        title: '选中的条目图标',
        name: 'menuItemSelectedIcon',
        group: groupName[0],
        help: 'VNode | slot'
      },
      {
        type: 'attr',
        name: 'tokenSeparators',
        group: groupName[0],
        help: 'string[]，在 tags 和 multiple 模式下自动分词的分隔符'
      },
      {
        type: 'model',
        title: '选中的条目',
        name: 'value'
      },
      {
        type: 'attr',
        title: '是否默认展开下拉菜单',
        name: 'defaultOpen',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'attr',
        title: '是否展开下拉菜单',
        name: 'open',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'event',
        title: '失去焦点时回调',
        name: 'blur'
      },
      {
        type: 'event',
        title: '值变化时回调',
        name: 'change',
        help: '选中 option，或 input 的 value 变化（combobox 模式下）时，调用此函数'
      },
      {
        type: 'event',
        title: '取消选中时回调',
        name: 'deselect',
        help: '参数为选中项的 value (或 key) 值，仅在 multiple 或 tags 模式下生效'
      },
      {
        type: 'event',
        title: '获得焦点时回调',
        name: 'focus'
      },
      {
        type: 'event',
        title: '键盘按下时回调',
        name: 'inputKeydown'
      },
      {
        type: 'event',
        title: '鼠标移入时回调',
        name: 'mouseenter'
      },
      {
        type: 'event',
        title: '鼠标移出时回调',
        name: 'mouseleave'
      },
      {
        type: 'event',
        title: '下拉列表滚动时的回调',
        name: 'popupScroll'
      },
      {
        type: 'event',
        title: '文本框值变化时回调',
        name: 'search'
      },
      {
        type: 'event',
        title: '被选中时调用',
        name: 'select',
        help: '参数为选中项的 value (或 key) 值'
      },
      {
        type: 'event',
        title: '展开下拉菜单的回调',
        name: 'dropdownVisibleChange'
      }
    ]
  }
}

export default data
