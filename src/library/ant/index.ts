import { ComponentGroupInfo, ConfigDetailType, InputListConfig } from '../page-object'
import general from './general'
import layout from './layout'
import dataEntry from './data-entry'
import dataDisplay from './data-display'

// 按照官网文档顺序排序，https://1x.antdv.com/docs/vue/introduce-cn/
const data: Array<ComponentGroupInfo> = [general, layout, dataEntry, dataDisplay]

export default data

// 获取一个Boolean的选择框
export function getBooleanSelect(trueLabel: string = '是', falseLabel: string = '否'): InputListConfig {
  return {
    title: 'Select',
    component: ConfigDetailType.Select,
    attr: {
      allowClear: true,
      options: [
        {
          value: '$:true',
          label: trueLabel
        },
        {
          value: '$:false',
          label: falseLabel
        }
      ]
    }
  }
}

// 获取一个Size的选择框
export function getSizeSelect(): InputListConfig {
  return {
    title: 'Select',
    component: ConfigDetailType.Select,
    attr: {
      allowClear: true,
      options: [
        {
          value: 'default',
          label: '默认'
        },
        {
          value: 'small',
          label: '小'
        },
        {
          value: 'large',
          label: '大'
        }
      ]
    }
  }
}
