import { ComponentGroupInfo } from '../../page-object'
import table from './table'

const data: ComponentGroupInfo = {
  id: 'data-display',
  title: '数据显示',
  list: [...table]
}

export default data
