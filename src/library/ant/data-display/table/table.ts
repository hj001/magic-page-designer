import { PageDom, ConfigDetailType } from '@/library/page-object'
import { getBooleanSelect } from '@/library/ant'

const groupName: Array<string> = ['样式']

const data: PageDom = {
  id: 'a-table',
  title: '表格',
  mpdConfig: {
    slot: []
  },
  ext: {
    componentConfig: [
      {
        type: 'attr',
        name: 'tableLayout',
        help: '表格元素的 table-layout 属性，设为 fixed 表示内容不会影响列的布局'
      },
      {
        type: 'attr',
        title: '是否展示边框',
        name: 'bordered',
        group: 'general',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'attr',
        title: '指定树形结构的列名',
        name: 'childrenColumnName',
        help: 'string[]'
      },
      {
        type: 'attr',
        title: '表格列的配置描述',
        name: 'columns',
        group: 'general',
        help: 'array',
        defaultValue: [],
        inputList: [
          {
            title: '表格弹窗',
            component: ConfigDetailType.ArrayTable,
            attr: {
              columns: [
                {
                  title: '列头显示文字(title)',
                  dataIndex: 'title',
                  scopedSlots: { customRender: 'titleSlot' }
                },
                {
                  title: '列数据key(dataIndex)',
                  dataIndex: 'dataIndex',
                  scopedSlots: { customRender: 'dataIndexSlot' }
                },
                {
                  title: '列宽度(width)',
                  dataIndex: 'width',
                  scopedSlots: { customRender: 'widthSlot' }
                }
              ]
            }
          }
        ]
      },
      {
        type: 'attr',
        title: '覆盖默认的table元素',
        name: 'components',
        help: 'object'
      },
      {
        type: 'attr',
        title: '数据数组',
        name: 'dataSource',
        group: 'general'
      },
      {
        type: 'attr',
        title: '初始展开所有行',
        name: 'defaultExpandAllRows',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'attr',
        title: '默认展开的行',
        name: 'defaultExpandedRowKeys',
        help: 'string[]'
      },
      {
        type: 'model',
        title: '展开的行',
        name: 'expandedRowKeys',
        help: '展开的行，控制属性。可用 .sync 后缀, 参见 update:expandedRowKeys'
      },
      {
        type: 'attr',
        title: '额外的展开行',
        name: 'expandedRowRender',
        help:
          'Function(record, index, indent, expanded):VNode | slot="expandedRowRender" slot-scope="record, index, indent, expanded"'
      },
      {
        type: 'attr',
        title: '自定义展开图标',
        name: 'expandIcon',
        help: 'Function(props):VNode | slot="expandIcon" slot-scope="props"'
      },
      {
        type: 'attr',
        title: '点击行展开子行',
        name: 'expandRowByClick',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'attr',
        title: '展开的图标显示在哪一列',
        name: 'expandIconColumnIndex',
        group: 'general',
        help: '如果没有 rowSelection，默认显示在第一列，否则显示在选择框后面',
        inputList: [
          {
            title: '数字框',
            component: ConfigDetailType.InputNumber
          }
        ]
      },
      {
        type: 'attr',
        title: '表格尾部',
        name: 'footer',
        help: 'Function(currentPageData)|slot-scope'
      },
      {
        type: 'attr',
        title: '每层缩进的宽度',
        name: 'indentSize',
        help: '展示树形数据时，每层缩进的宽度，以 px 为单位',
        inputList: [
          {
            title: '数字框',
            component: ConfigDetailType.InputNumber
          }
        ]
      },
      {
        type: 'attr',
        title: '页面是否加载中',
        name: 'loading',
        group: 'general',
        help: 'boolean|object'
      },
      {
        type: 'attr',
        title: '默认文案',
        name: 'locale',
        help: '默认文案设置，目前包括排序、过滤、空数据文案，object'
      },
      {
        type: 'attr',
        title: '分页器',
        name: 'pagination',
        group: 'general',
        help: '参考配置项或 pagination文档，设为 false 时不展示和进行分页'
      },
      {
        type: 'attr',
        title: '表格行的类名',
        name: 'rowClassName',
        help: 'Function(record, index):string'
      },
      {
        type: 'attr',
        title: '表格行key的取值',
        name: 'rowKey',
        group: 'general',
        help: 'string|Function(record):string'
      },
      {
        type: 'attr',
        title: '列表项是否可选择',
        name: 'rowSelection'
      },
      {
        type: 'attr',
        title: '设置横向或纵向滚动',
        name: 'scroll',
        group: 'general',
        help:
          '也可用于指定滚动区域的宽和高，建议为 x 设置一个数字，如果要设置为 true，需要配合样式 .ant-table td { white-space: nowrap; }，类型：{ x: number | true, y: number }'
      },
      {
        type: 'attr',
        title: '是否显示表头',
        name: 'showHeader',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'attr',
        title: '表格大小',
        name: 'size',
        group: 'general',
        inputList: [
          {
            title: 'Select',
            component: ConfigDetailType.Select,
            attr: {
              allowClear: true,
              options: [
                {
                  value: 'default',
                  label: '默认'
                },
                {
                  value: 'small',
                  label: '小'
                },
                {
                  value: 'middle',
                  label: '中'
                }
              ]
            }
          }
        ]
      },
      {
        type: 'attr',
        title: '表格标题',
        name: 'title',
        help: 'Function(currentPageData)|slot-scope'
      },
      {
        type: 'attr',
        title: '设置头部行属性',
        name: 'customHeaderRow',
        help: 'Function(column, index)'
      },
      {
        type: 'attr',
        title: '设置行属性',
        name: 'customRow',
        help: 'Function(record, index)'
      },
      {
        type: 'attr',
        title: '渲染节点',
        name: 'getPopupContainer',
        help: '设置表格内各类浮层的渲染节点，如筛选菜单，类型：(triggerNode) => HTMLElement'
      },
      {
        type: 'attr',
        name: 'transformCellText',
        help:
          '	数据渲染前可以再次改变，一般用户空数据的默认配置，可以通过 ConfigProvider 全局统一配置，类型：Function({ text, column, record, index }) => any'
      },
      {
        type: 'event',
        name: 'change',
        help: '分页、排序、筛选变化时触发，类型：Function(pagination, filters, sorter, { currentDataSource })'
      },
      {
        type: 'attr',
        title: '点击展开图标',
        name: 'expand',
        help: 'Function(expanded, record)'
      },
      {
        type: 'attr',
        title: '展开的行变化',
        name: 'expandedRowsChange',
        help: 'Function(expandedRowKeys)'
      }
    ]
  }
}

export default data
