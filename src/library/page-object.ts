// 组件分组对象
export interface ComponentGroupInfo {
  // 组件id,应该唯一
  id: string
  // 组件库标题
  title: string
  // 可用组件列表
  list: Array<PageDom>
}
// page-dom配置项
export interface PageDom {
  // id唯一标识(可用组件名),拖拽到页面后由uuid重新生成
  id: string
  // 节点名称
  title: string
  // 页面dom配置
  mpdConfig?: MpdConfig
  // 扩展属性,仅页面展示使用,拖拽后删除属性
  ext?: ComponentItemExt
  // 子节点
  children?: Array<PageDom>
}
// 页面dom配置
export interface MpdConfig {
  // 使用的组件名,作为组件库元素使用时,可以不填,拖拽到页面后如果为空,则取PageDom的id
  component?: string
  // 双向绑定属性
  model?: { [key: string]: any }
  // 传递的属性
  attr?: { [key: string]: any }
  // class
  class?: string
  // style
  style?: Array<KeyValue>
  // v-if
  vIf?: string
  // v-show
  vShow?: string
  // v-on绑定的事件
  event?: { [key: string]: (...arg: any) => void }
  // text内容
  text?: string
  // slot内容
  slot?: Array<PageDom>
  // 记录一些属性用的配置，用于设计器配置
  configTmp?: { [key: string]: any }
}
// style使用的配置项
export interface KeyValue {
  key: string
  value: string | number
}
// 组件库扩展属性,不会放置于dom对象中
export interface ComponentItemExt {
  // 展示所需要的图标
  icon?: string
  // 帮助信息
  help?: string
  // 子节点支持拖拽的白名单，为undefined则不支持添加子节点，如果字段存在，并且长度为0，则支持所有的组件
  childrenComponent?: Array<string> | undefined | null
  // 支持拖入父节点的白名单，为undefined、长度为0、null则不受控制，如果配置的有对应组件，就会进行父组件验证
  parentComponent?: Array<string> | undefined | null
  // 组件自己的配置
  componentConfig?: Array<ComponentConfig>
}

// 配置信息，主要记录v-bind，v-on等相应的组件配置
export interface ComponentConfig {
  type: 'attr' | 'event' | 'model'
  // 配置的字段标题
  title?: string
  // 配置的字段名
  name: string
  // 默认值
  defaultValue?: any
  // 分组，相同分组的放到同一个tab下，只有在type='attr'时生效，如果为general则会放到常规配置里面
  group?: 'general' | string
  // 帮助信息
  help?: string
  // 可配置列表
  inputList?: Array<InputListConfig>
}
export interface InputListConfig {
  // 组件标题
  title?: string
  // 配置类型，默认会添加一个a-textarea
  component: ConfigDetailType
  // attr中的value，如果是"$:"开头会放入到eval中执行
  attr?: { [key: string]: any }
  event?: { [key: string]: Function }
}
// 配置详情类型
export enum ConfigDetailType {
  Input = 'a-input',
  Select = 'a-select',
  Textarea = 'a-textarea',
  InputNumber = 'a-input-number',
  ArrayTable = 'mpd-array-table'
}
