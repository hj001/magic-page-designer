import { ComponentGroupInfo } from './page-object'
import { TextTypeEnum } from '../components/mpd-text/text-object'
import { ConfigDetailType } from './page-object'

const mpdTextOptions = []
for (const key in TextTypeEnum) {
  mpdTextOptions.push({
    value: TextTypeEnum[key],
    label: key
  })
}

const data: ComponentGroupInfo = {
  id: 'text',
  title: '文本',
  list: [
    {
      id: 'mpd-text',
      title: 'Text',
      mpdConfig: {
        text: '在此处修改mpd-text标签内容'
      },
      ext: {
        icon: 'mpd-icon-text-border',
        componentConfig: [
          {
            type: 'attr',
            title: '文本标签类型',
            name: 'type',
            group: 'general',
            defaultValue: 'p',
            inputList: [
              {
                title: 'Select',
                component: ConfigDetailType.Select,
                attr: {
                  options: mpdTextOptions
                }
              }
            ]
          }
        ]
      }
    },
    {
      id: 'mpd-icon',
      title: '字体图标',
      ext: {
        icon: 'mpd-icon-picture',
        componentConfig: [
          {
            type: 'attr',
            title: '图标class',
            name: 'icon',
            group: 'general'
          },
          {
            type: 'attr',
            title: '大小',
            name: 'fontSize',
            group: 'general',
            inputList: [
              {
                title: '数字框',
                component: ConfigDetailType.InputNumber
              }
            ]
          }
        ]
      }
    }
  ]
}
export default data
