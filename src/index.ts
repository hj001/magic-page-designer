import MagicPageDesigner from './views/MagicPageDesigner.vue'
import PageNodeDom from './views/workspace/page-preview/PageNodeDom.vue'
import { addLibrary } from '@/store/page-config'
import { mpdMessage } from './util/messageUtil'
import Vue from 'vue'
import library from './library'

// import 'default-passive-events'
// 自定义组件
import CustomComponents from './components'
// 引入ant
import ImportAnt from './import-ant'
// 最后引入自定义css，为了覆盖其它样式
import '@/assets/index'

const install = function(app: typeof Vue) {
  app.component(PageNodeDom.name, PageNodeDom)
  app.component(MagicPageDesigner.name, MagicPageDesigner)
  // 自定义组件
  app.use(CustomComponents)
  // 引入ant
  app.use(ImportAnt)
  // 引入提示信息
  app.prototype.$mpdMessage = mpdMessage
}

export { addLibrary }
export * from './library'
// 添加ant组件库配置
export function addAntLibrary(): void {
  addLibrary(library.getAntConfig())
}

export default {
  install,
  addLibrary,
  ...library,
  addAntLibrary
}
